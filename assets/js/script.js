// patient record system
// record all patients
const patients = [];
let id = 0;

// 1.  register function will add a person in the list of patients
// a patient should have
// id
// name
// age
// status
// method to change status
// id should start with 0
// age should be a number
// if the registration is successful it will log in console:
// "Patient <number> is added to the record"
const register = () => {
	const name = prompt('What is the name?');
	let age = Number(prompt('What is the age?'));
	patients.push({ id: id, name: name, age: age, status: 'healthy' });
	id++;
	console.log(`Patient ${id} is added to the record`);
};

// 2. patients function will log all the patients registered
// [
// 	{
// 		id: patient's id
// 		age: patient's age
// 		status: patient's status
// 	},
// 	{
// 		id: patient's id
// 		age: patient's age
// 		status: patient's status
// 	},
// 	{
// 		id: patient's id
// 		age: patient's age
// 		status: patient's status
// 	},
// ]
const showPatients = () => {
	const ppl = [];
	patients.forEach((item) => {
		ppl.push({ id: item.id, age: item.age, status: item.status });
	});
	console.log(ppl);
};
// 3. changePersonStatus
// first ask the user the patient's number
// only accepts number
// ask the user what will be the new status
// update the patient's status
// log in console
// "Patient <number> is now <status>"
const changePersonStatus = () => {
	const index = Number(prompt('What Patient do you want to change status?'));
	const newStatus = prompt('What is the new status?');
	patients[index].status = newStatus;
	console.log(`Patient with id:${patients[index].id} is now ${newStatus}`);
};
